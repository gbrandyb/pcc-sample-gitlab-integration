#!/bin/bash
echo "Make sure nc command is available"
if ! [ -x "$(command -v nc)" ]; then
  echo "Installing netcat"
  #apt-get update && apt list netcat-traditional && apt-get install -y netcat-traditional
  apt-get update
  apt-get install -y netcat-traditional
else
  echo "Netcat already installed"
fi
# now let's loop this forever so we can actually deploy it 
# and have it run in the wild for PCC to protect
while true
do 
  if [ -n "$1" -a "$1" == "bad" ]; then   
    ## -- if we pass the "bad" flag we will run the malware -- ##
    echo "Trigger Wildfire malware"
    /bin/malware
    echo "Trigger dropper"
    cp /bin/invoker_arm64 /bin/invoker_arm64_1
    echo "Trigger suspicous elf"
    /bin/invoker_arm64_1
    cat invoker_arm64_1 > /dev/null
    echo "Trigger cryptominer"
    /bin/xmrig
    echo "Run go program that makes some network traffic and triggers more findings"
    /bin/invoker_amd64
    echo "Trigger modified binary execution"
    /bin/sleep 0.2s
    echo "Trigger DNS query from a non-go app"
    nc github.com 443
  fi
  echo "Finished all the bad stuff now go to sleep for a minute & repeat"
  sleep 60
done