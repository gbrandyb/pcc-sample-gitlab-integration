{{- define "genImagePullSecret" }}
{{- with .Values.imageCredentials }}
{{- printf "{\"auths\":{\"%s\":{\"auth\":\"%s\"}}}" .registry (printf "%s:%s" .user .token | b64enc) | b64enc }}
{{- end }}
{{- end }}

{{- define "genImagePullName" }}
{{- with .Values.imageDetails }}
{{- printf "%s/%s:%s" .registryImage .imageName .imageTag }}
{{- end }}
{{- end }}