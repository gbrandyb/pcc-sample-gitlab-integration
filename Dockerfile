# 14.04 - 1 crit, 12 highs (CVE-2020-1971 is not in 22.10. Use as exception to trigger)
# 22.04 - 1 crit, 6 high
FROM  ubuntu:22.04
LABEL env="dev"
COPY ./elf /bin/malware
COPY ./entrypoint.sh /bin/entrypoint.sh
COPY ./invoker_amd64 /bin/invoker_amd64
COPY ./invoker_arm64 /bin/invoker_arm64
COPY ./invoker_386 /bin/invoker_386
COPY ./xmrig /bin/xmrig
RUN chmod +x /bin/malware
RUN chmod +x /bin/entrypoint.sh
ENTRYPOINT ["/bin/entrypoint.sh", "bad"]
