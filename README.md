# Twistlock Sample CICD

Will build a docker image containing Go program, C program, several command line calls and various other "badboy" activities.

The pipeline will:

Build
  - Compile the go and C executables. (files: xmrig.c,main.go jobfiles: jobs/.gitlab-ci-compile.yml)
  - Build the docker image (files:entrypoint.sh, Dockerfile jobfiles: jobs/.gitlab-ci-build.yml)
  - Scan the image with PCC and if successful push to the registry (jobfiles: jobs/.gitlab-ci-scancontainer.yml)

Test
  - Run sandbox analysis (jobfiles: jobs/.gitlab-ci-scancontainer.yml)

Deploy
  - Deploy into cluster via Helm chart (files: Values.yaml, Chart.yaml, templates/* jobfiles: jobs/.gitlab-ci-kubedeploy.yml)
  

Demo Notes:
  - Image Vulnerability Scan
    - The following assumes a CI vulnerability that Fails on Critical CVEs and has the following two exceptions:
      - CVE-2022-23806 Effect: Alert (this is a critical CVE that is present in both images listed below)
      - CVE-2020-1971 Effect: Fail (this is a high alert only present in 14.04 not 22.10) 
    - To fail the image scan (CVEs, compliance) modify Dockerfile => change base image to be 14.04
    - To pass the image scan (CVEs, compliance) modify Dockerfile => change base image to be 22.10
  - Image Sandbox Scan    
    - To fail the sandbox scan modify Dockerfile => change ENTRYPOINT ["/bin/entrypoint.sh", "XXXX"] to ENTRYPOINT ["/bin/entrypoint.sh", "bad"]
    - To pass the sandbox scan modify Dockerfile => change ENTRYPOINT ["/bin/entrypoint.sh", "XXXX"] to ENTRYPOINT ["/bin/entrypoint.sh", "good"]