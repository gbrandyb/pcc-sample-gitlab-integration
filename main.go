package main

import (
    "os"
    "net"
    "net/http"
    "fmt"
    "golang.org/x/sys/unix"
    "unsafe"
)

func main() {
    memfd, err := unix.MemfdCreate("", unix.O_WRONLY)
    if err!=nil {
        panic(err)
    }
    if _,err:=unix.Write(memfd, []byte("#!/bin/bash")); err!= nil {
        panic(err)
    }
    defer unix.Close(memfd)
    unix.InitModule(nil,"")
    for i := 0; i< 20; i++ {
        net.DialUDP("udp", nil, &net.UDPAddr{Port: 5000 + i, IP: net.ParseIP("1.1.1.1")})
    }
    net.ListenTCP("tcp",&net.TCPAddr{Port: 4000})
    http.Get("https://pkg.go.dev")
    f,_:=os.OpenFile("/bin/sleep", os.O_WRONLY|os.O_APPEND, 0700)
    f.Write([]byte("altering"))
    f.Close()
    empty := []byte{0}
    argvp := []*byte{&empty[0]}
    envvp := []*byte{&empty[0]}

    // Execution will fail because the memfd file contains a script, not a binary - but it will still trigger attempt
    unix.RawSyscall6(unix.SYS_EXECVEAT,
        uintptr(memfd),
        uintptr(unsafe.Pointer(&empty[0])),
        uintptr(unsafe.Pointer(&argvp[0])),
        uintptr(unsafe.Pointer(&envvp[0])),
        uintptr(unix.AT_EMPTY_PATH), 0)
    fmt.Printf("%s done\n", os.Args[0])
}