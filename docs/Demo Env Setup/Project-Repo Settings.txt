Project Variables (none are protected) (Project Settings -> CI/CD -> Variables)
  - pcc_username                   -> PCC user
  - pcc_password                   -> PCC passwd  (masked)
  - pcc_url                        -> PCC console Address
  
  ### the following variables used to be Project Variables but are now defined in the base level gitlab-ci.yaml
  # pcc_prod_cluster               -> Production kubernetes cluster name. Should be setup for integration per Gitlab Kubernetes Integration.txt
  # pcc_img_name                   -> Name to use for the image being built
  #- pcc_img_tag                    -> Tag to use for the image being built
  # pcc_helm_deployment_name       -> name for this specific helm deployment

Runners  
  - 1 shared tagged with docker, shell.
    - also edit config to allow it to pick up untagged jobs 
  -1 shared tagged with sandbox
    - ensure it DOES NOT pick up untagged jobs
